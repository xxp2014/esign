<?php
return [
    'frame' =>  'tp6',
    'appId' => '7438825409',
    'appSecret' => 'fa3d990998a4311edb5c2223e702d33a',
    'apiConfig' => [
        'host' => 'https://smlcunzheng.tsign.cn:9443'
    ],
    'dbModel' => [
        'evidenceCategoryModel' => \app\model\esign\evidence\EvidenceCategoryModel::class,
        'evidencePointModel' => \app\model\esign\evidence\EvidencePointModel::class,
        'evidenceSceneModel' => \app\model\esign\evidence\EvidenceSceneModel::class,
        'evidenceChainPointModel' => \app\model\esign\evidence\EvidenceChainPointModel::class,
        'evidenceChainModel' => \app\model\esign\evidence\EvidenceChainModel::class,
    ],
    'evidence' =>
        [
            [
                'category_code' => 'medicine',
                'name' => '医药',
                'scenes' => [
                    [
                        'scene_code' => 'task',
                        'name' => '任务',
                        'points' => require_once 'Evidence/Property/task.php',
                    ],
                ]
            ]

        ]
];