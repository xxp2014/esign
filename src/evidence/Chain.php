<?php
namespace Soen\Esign\Evidence;

use Soen\Esign\Config;
use Soen\Esign\Evidence\Adapter\AdapterInterface;
use Soen\Esign\Evidence\Adapter\Tp\TpAdapter;
use Soen\Esign\Exception\EviException;
use Soen\Esign\Http;
use think\Model;


class Chain
{
	public $http;
	public $eviConfig;
    /**
     * @var AdapterInterface
     */
	public $adapter;
	public $actCreatePointEviCode;
	public $actCreateSceneCode;
	public $actCreateSceneEviCode;
    /**
     * @var array
     */
    public $currentSceneRow;
	public $currentChain;
	public $currentChainPoint;
	public function __construct(Http $http,Config $config,$models)
	{
		$this->http = $http;
		$this->eviConfig = $config->getEviConfig();
        if($this->eviConfig['frame'] == 'tp6'){
            $this->adapter = new TpAdapter($this->http, $config, $models);
        }
	}

	public function dictionary()
    {
        $this->adapter->evidenceMap();
        $this->adapter->setEsignCode();

    }

    public function setActCreatePointEviCode($actCreatePointCode)
    {
        $this->actCreatePointEviCode = $this->adapter->getPointEviCode($actCreatePointCode);
        return $this;
    }


    /**
     * @param $pointCode
     * @return mixed
     */
    public function getPointEviCode($pointCode)
    {
        return $this->adapter->getPointEviCode($pointCode);
    }

    /**
     * @param $segmentTempletId
     * @param $segmentData
     * @param $logicData
     * @return mixed
     */
    public function createChainPoint($propertyContent,$logicData,$content = [])
    {
		$result = $this->http->post('segmentOriginalAdv',[
			'segmentTempletId'  =>  $this->actCreatePointEviCode,
			'segmentData'   =>  json_encode($propertyContent,JSON_UNESCAPED_UNICODE),
            'content'   =>  $content
		]);
        //数据库保存
        $currentChainPoint = $this->adapter->createChainPoint($result,$propertyContent,$this->actCreatePointEviCode,$logicData);
        $this->currentChainPoint = $currentChainPoint;
        //注入当前场景code
        $this->actCreateSceneCode = $this->actCreateSceneCode ?? $this->currentChainPoint['scene_code'];
		return $result;
    }

    /**
     * @param string $sceneCode
     * @param string $logicUniqueCode
     * @return mixed
     */
    public function createDbChain($sceneCode = '', $logicUniqueCode = '')
    {
        //注入当前场景code
        $this->actCreateSceneCode = $this->actCreateSceneCode ?? $sceneCode;
        $evidenceSceneModelRow = $this->adapter->evidenceSceneModel->where('scene_code',$sceneCode)->find();
        $this->actCreateSceneEviCode = $evidenceSceneModelRow->e_scene_code;
        $this->currentSceneRow = $evidenceSceneModelRow;
        $this->currentChain = $this->adapter->createChain($this->actCreateSceneCode,$logicUniqueCode,$this->actCreateSceneEviCode);
        return $this->currentChain['id'];
    }

    public function createEviChain($pointEviCodes = '')
    {
        $pointEviCodes = $pointEviCodes ?: $this->adapter->getPointEviCodesForCreatEviChain();
        $result = $this->http->post('createEviChain',[
            'sceneName' =>  $this->currentSceneRow['name'],
            'sceneTemplateId'   =>  $this->actCreateSceneEviCode,
            'linkIds'   =>  $pointEviCodes
        ]);
        p($result);
        //更新证据链evid
        $this->adapter->updateChain(['e_chain_evid' => $result['evid']]);
    }

    public function getCurrentChain($logicUniqueCode = '')
    {
        return $this->adapter->getCurrentChainModel($logicUniqueCode);
    }

    /**
     * @param string $logicUniqueCode
     * @return $this
     */
    public function setCurrentChain($logicUniqueCode = '')
    {
        $this->currentChain = $this->getCurrentChain($logicUniqueCode);
        $this->actCreateSceneEviCode = $this->currentChain->e_scene_code;
        $this->actCreateSceneCode = $this->currentChain->scene_code;
        //注入当前场景数据
        $this->currentSceneRow = $this->adapter->getSceneRow($this->currentChain->scene_code);
        return $this;
    }


    public function appendEviPoint($linkIds,$eChainEvid = '')
    {
        $eChainEvid = $eChainEvid ?: $this->currentChain['e_chain_evid'];
        $this->http->post('appendEviPoint',[
            'evid'      =>  $eChainEvid,
            'linkIds'   =>  $linkIds
        ]);
    }

    public function getChainInfo($eid)
    {
        $result = $this->http->post('getChainInfo',['eid' => $eid]);
        return $result;
    }






}