<?php


namespace Soen\Esign\Evidence\Adapter;


interface AdapterInterface
{
    public function evidenceMap();
    public function setEsignCode();
//    public function createCategory();
//    public function createScene($categoryCode, $eCategoryCode);
//    public function createPoint($sceneCode,$eSceneCode,$pointArr);
}