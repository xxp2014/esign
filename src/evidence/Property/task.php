<?php
//据点列表
return [
	[
		'point_code' => 'taskPublish',
		'name' => '任务发布',
		'properties' => [
			[
				'displayName' => '任务名称',
				'paramName' => 'taskName'
			],
			[
				'displayName' => '任务ID',
				'paramName' => 'taskId'
			],
            [
                'displayName' => '任务详细',  //json
                'paramName' => 'taskDetail'
            ],
            [
                'displayName' => '任务要求',
                'paramName' => 'taskDemand'
            ],
            [
                'displayName' => '任务金额',
                'paramName' => 'taskMoney'
            ],
//            [
//                'displayName' => '企业剩余金额',
//                'paramName' => 'companyMoney'
//            ],
//            [
//                'displayName' => '企业名称',
//                'paramName' => 'companyName'
//            ],
			[
				'displayName' => '企业详细',
				'paramName' => 'companyDetail'
			],
            [
                'displayName' => '发布时间',
                'paramName' => 'createTime'
            ],
		]
	],
	[
		'point_code' => 'winBid',
		'name' => '任务中标',
		'properties' => [
			[
				'displayName' => '中标时间',
				'paramName' => 'createTime'
			],
            [
                'displayName' => '中标企业',
                'paramName' => 'bidWinCompanyName'
            ],
			[
				'displayName' => '中标公司详细',
				'paramName' => 'bidWinCompanyDetail'
			]
		]
	],
    [
        'point_code' => 'taskAssign',
        'name' => '任务分配',
        'properties' => [
            [
                'displayName' => '分配时间',
                'paramName' => 'createTime'
            ],
            [
                'displayName' => '分配详细',
                'paramName' => 'assignDetail'
            ]
        ]
    ],
	[
		'point_code' => 'taskSubmit',
		'name' => '任务提交',
		'properties' => [
			[
				'displayName' => '提交时间',
				'paramName' => 'createTime'
			],
            [
                'displayName' => '任务完成详细',
                'paramName' => 'taskFinishDetail'
            ],
            [
                'displayName' => '提交人详细',
                'paramName' => 'submitterDetail'
            ]
		]
	],
    [
        'point_code' => 'taskEvaluate',
        'name' => '任务评价',
        'properties' => [
            [
                'displayName' => '评价者姓名',
                'paramName' => 'evaluatorName'
            ],
            [
                'displayName' => '评价者详细',
                'paramName' => 'evaluatorDetail'
            ],
            [
                'displayName' => '评价时间',
                'paramName' => 'createTime'
            ],
            [
                'displayName' => '评价详细',
                'paramName' => 'evaluateDetail'
            ],
            [
                'displayName' => '评价总金额',
                'paramName' => 'evaluateMoney'
            ],
            [
                'displayName' => '企业剩余金额',
                'paramName' => 'companyMoney'
            ]
        ]
    ],
	[
		'point_code' => 'allocateMoney',
		'name' => '资金分配',
		'properties' => [
			[
				'displayName' => '分配详细', //json
				'paramName' => 'allocateMoneyDetail'
			]
		]
	],
    [
        'point_code' => 'allocateMoneyArrival',
        'name' => '分配资金发放',
        'properties' => [
            [
                'displayName' => '分配资金发放详细', //json
                'paramName' => 'allocateMoneyArrival'
            ]
        ]
    ]
];