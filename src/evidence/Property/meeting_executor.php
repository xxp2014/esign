<?php
//据点列表
return [
	[
		'point_code' => 'taskAccept',
		'point_name' => '被分配任务',
		'properties' => [
			[
				'displayName' => '安排时间',
				'paramName' => 'createTime'
			],
			[
				'displayName' => '任务ID',
				'paramName' => 'taskId'
			],
			[
				'displayName' => '任务名称',
				'paramName' => 'taskName'
			],
			[
				'displayName' => '任务详细ID',
				'paramName' => 'taskDetilId'
			],
			[
				'displayName' => '推荐人ID',
				'paramName' => 'referrerUserId'
			],
			[
				'displayName' => '推荐人姓名',
				'paramName' => 'referrerUserName'
			]
		]
	],
	[
		'point_code' => 'taskExecuteSubmit',
		'point_name' => '任务执行提交',
		'properties' => [
			[
				'displayName' => '提交时间',
				'paramName' => 'createTime'
			],
			[
				'displayName' => '任务ID',
				'paramName' => 'taskId'
			],
			[
				'displayName' => '任务详细ID',
				'paramName' => 'taskDetailId'
			]
		]
	],
	[
		'point_code' => 'getLabourFee',
		'point_name' => '被分配劳务费',
		'properties' => [
			[
				'displayName' => '分配时间',
				'paramName' => 'createTime'
			],
			[
				'displayName' => '分配金额',
				'paramName' => 'fee'
			],
			[
				'displayName' => '任务ID',
				'paramName' => 'taskId'
			]
		]
	]
];