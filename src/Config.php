<?php


namespace Soen\Esign;


class Config
{
	public $eviConfig;
	public function __construct($path = '')
	{
		$this->eviConfig = require_once $path . 'evidence.php';
	}

	public function getEviApiHost()
	{
		return $this->eviConfig['apiConfig']['host'];
	}

	public function getEviAppId()
    {
        return $this->eviConfig['appId'];
    }

    public function getEviAppSecret()
    {
        return $this->eviConfig['appSecret'];
    }

	public function getEviConfig()
	{
		return $this->eviConfig;
	}


}