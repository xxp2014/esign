<?php
return [
    'appId' =>  '7438825409',
    'appSecret' =>  'fa3d990998a4311edb5c2223e702d33a',
	'apiConfig' => [
		'host' => 'https://smlcunzheng.tsign.cn:9443'
	],
	'dbModel' => [
		'evidenceCategoryModel' => \app\model\esign\EvidenceCategoryModel::class,
		'evidencePointModel' => \app\model\esign\EvidencePointModel::class,
		'evidenceSceneModel'    =>  \app\model\esign\EvidenceSceneModel::class
	],
	'evidence' =>
		[
			'category_code' => 'medicine',
			'category_name' => '医药',
			'scenes' => [
				[
					[
						'scene_code' => 'task',
						'scene_name' => '任务',
						'points' => require_once './evidence/properties/task.php',
					],
					[
						'id' => 'executor',
						'scene_name' => '执行者',
						'points' => require_once './properties/meeting_executor.php',
					],
	//				[
	//					'id' => 'business',
	//					'scene_name' => '企业',
	//					'points' => require_once './properties/meeting_referrer.php',
	//				]
				]
			]
		]
];