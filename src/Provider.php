<?php


namespace Soen\Esign;


use Soen\Esign\Evidence\Chain;

class Provider
{
	public $http;
	public $config;
	public function __construct($path = '')
	{
		$this->config = new Config($path);
		$this->http = new Http($this->config);
//		$this->evidenceCategoryModel = new $this->config['dbModel']['evidenceCategoryModel'];
//		$this->evidencePointModel = new $this->config['dbModel']['evidencePointModel'];
//		$this->evidenceSceneModel = new $this->config['dbModel']['evidenceSceneModel'];
	}

	public function objectEvidence()
	{
        $evidenceCategoryModel = $this->config->getEviConfig()['dbModel']['evidenceCategoryModel'];
        $evidencePointModel = $this->config->getEviConfig()['dbModel']['evidencePointModel'];
        $evidenceSceneModel = $this->config->getEviConfig()['dbModel']['evidenceSceneModel'];
        $evidenceChainPointModel = $this->config->getEviConfig()['dbModel']['evidenceChainPointModel'];
        $evidenceChainModel = $this->config->getEviConfig()['dbModel']['evidenceChainModel'];
		return new Chain($this->http,$this->config,[
            'evidenceCategoryModel' =>  new $evidenceCategoryModel,
            'evidencePointModel'    =>  new $evidencePointModel,
            'evidenceSceneModel'    =>  new $evidenceSceneModel,
            'evidenceChainPointModel' => new $evidenceChainPointModel,
            'evidenceChainModel'    =>  new $evidenceChainModel
        ]);
	}
}